#!/usr/bin/env python

'''
This script gathers "lock status -c" & "lock status -v" outputs from NTAP 7m systems.
It then checks if the "In-Use Owners" is high, exits script if "In-Use Owners" is low.  If it is high,
it then checks if "Closed StateID count" is low, if it is high, it exits the script.  If it is low,
then checks the "lock status -v" output to find the clients with the highest lock usage and sends out notifications.
'''

from __future__ import print_function
import sys, subprocess, datetime, os.path, time, re, fileinput
from subprocess import Popen,PIPE 
start_time=datetime.datetime.today().strftime("%Y_%m_%d-%H_%M_%S")

# please update the variables below accordingly
out_path="/tmp/nfslockstatus/"
outfile_v = out_path+'nfslockstatusV_ker_7m01_2018_02_05-13_11_21.txt'

outfile_client_total = out_path+'ClientLeaseCount_overall.csv'
outfile_client = out_path+'ClientLeaseCount_'+start_time+'.csv'
outfile_closed_total = out_path+'ClientLeaseCount_closed_total.csv'
status_log = out_path + 'nfslockstatus_log.log'


if not os.path.exists(out_path):
    try:
    	o = os.makedirs(out_path, mode=764)
    except:
    	print ('\x1b[0;37;41m' + 'Failed to create the output diretory' + '\x1b[0m')
    	print("Please verify the user has permission to create directory in {0} and re-run the script.\n".format(out_path))
    	print('\x1b[0;37;41m' + 'Existing the script....' + '\x1b[0m' + '\n')
    	exit()

print('{0}:  lock status -v output file: {1}'.format(start_time,outfile_v), file=open(status_log,'a'))






# function for getting lock counts
def get_lock(file): 
    lock_count = {}
    ip_addr = '0.0.0.0'
    count = 0

    #Get important line numbers
    with open(file, 'r') as f:
        for i, line in enumerate(f):
            if 'Clients Dump' in line:
                startline = i
            
            if 'Owners Dump' in line:
                stopline = i
                
    #Add IP and Lease count to dictionary
    with open(file, 'r') as f:
        for i,line in enumerate(f):
            if i > startline and i < stopline:
                if "Id String" in line:
                    if re.findall('Linux', line):
                        ip_addr = (line.split()[4])
                    else:
                        ip_addr = (line.split()[2])
                if "Lease Count" in line:
                    count = int((line.split()[7]))
                else:
                    continue
                lock_count[ip_addr] = count
    lock_count=sorted(lock_count.items(), key = lambda x:x[-1], reverse = True)
    return lock_count


lock_count = get_lock(outfile_v)
for i in lock_count:
    print(start_time+','+str(i[0])+','+str(i[1]),file=open(outfile_client_total,'a'))

time.sleep(5)


# extract the number of closed ID the linux/unix had.
closed_linenum = []
client_linenum = []
with open(outfile_v, 'r') as f:
    for i,line in enumerate(f):
        if "Mode: Closed" in line:
            closed_linenum.append(i)
        if "Id String:" in line:	
            client_linenum.append(i)
            
closed_id = []
client_ip = {}
closed_count = {}
with open(outfile_v, 'r') as f:
    s = f.readlines()
    for i in closed_linenum:
        closed_id.append(s[i+2].split()[-1])
    for i in client_linenum:
        if re.findall('Linux', s[i]):
            client_ip[s[i-1].split()[2]] = str(s[i].split()[4])
        else:
            client_ip[s[i-1].split()[2]] = str(s[i].split()[2])
for key,val in client_ip.items():
    closed_count[val] = closed_id.count(key)
closed_count_d=sorted(closed_count.items(), key = lambda x:x[-1], reverse = True)

for i in closed_count_d:
    print(start_time+','+str(i[0])+','+str(i[1]),file=open(outfile_closed_total,'a'))


print('Parsing output has completed.  Existing the script....' + '\x1b[0m' + '\n')

