from lockobjects import ClientOwners
import sys

# Create new instance of base class
data = ClientOwners(sys.argv[1])

# Create the client id --> owners dictionary
owners = data.clienthex_2_owners()

# create the client id --> ip dictionary
clients = data.clienthex_2_ips()

# Iterate through the owners dictionary to find client ids with more than 1000 owners
for key, value in owners.items():
    if len(value) > 250:
        # pass client id as key to clients dictionary, and print how many owners associated with that client ip
        print("{a}: {b} owners".format(a=clients[key],b=len(value)))

