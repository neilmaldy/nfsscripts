'''
Author: Elliott Ecton <elliott.ecton@netapp.com>
Version: 0.1
Python required: 3.5+
Future Features: Python2.x instead of 3; parse hostnames correctly
'''
from collections import defaultdict

class ClientOwners(object):

    def __init__(self, filename):
        self.filename = filename

    def clienthex_2_owners(self):

        client_dict = defaultdict(list)
        startStateId = "StateId Dump"
        startClientsDump = 'Clients Dump'
        start_processing = False

        with open(self.filename, 'r') as f:
            fh = f.readlines()
            for i, entry in enumerate(fh):
                if startStateId in entry:
                    start_processing = True
                if startClientsDump in entry:
                    start_processing = False
                if start_processing == True and "Client Id" in entry:
                    client_x = entry.split()[2]
                    try:
                        owner_x = (fh[i - 2].split()[9])
                        if owner_x not in client_dict[client_x]:
                            client_dict[client_x].append(owner_x)

                    except:
                        pass
        return client_dict

    def clienthex_2_ips(self):

        start_at = 'Clients Dump'
        stop_at = 'Owners Dump'
        clienthex2ip = {}
        start_processing = False

        with open(self.filename, 'r') as f:
            fh = f.readlines()
            for i, entry in enumerate(fh):
                if start_at in entry:
                    start_processing = True
                if stop_at in entry:
                    start_processing = False
                if start_processing == True and "Client Id" in entry:
                    client_id = entry.split()[2]
                    next_line = fh[i + 1]
                    if "Solaris:" in next_line.split()[2] or "UNIX" in next_line.split()[2]:
                        client_ip = next_line.split()[3]
                        clienthex2ip[client_id] = client_ip
                    elif "Linux" not in next_line:
                        client_ip = next_line.split()[2]
                        clienthex2ip[client_id] = client_ip
                    else:
                        client_ip = next_line.split()[4]
                        clienthex2ip[client_id] = client_ip
        return clienthex2ip