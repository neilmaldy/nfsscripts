import sys
import getopt


def usage():
    pass


def main():

    input_file = 'nfslockstatusV_ker_7m01_2018_02_05-13_11_21.txt'
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:", ["help", "input="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            input_file = arg

    f = open(input_file)
    lines = f.read().splitlines()
    f.close()

    have_new_owner_id = False
    owners = []
    owner_id = ''
    last_mode = {}
    all_modes = {}
    closed_owners = []
    unique_modes = set()
    unique_owners = set()
    unique_owner_ids = set()
    unique_closed_owners = set()
    just_closed = set(['Closed;'])
    active_owner_ids = set()

    for line in lines:
        if have_new_owner_id and 'Type: 105' in line:
            have_new_owner_id = False
            owners.append(owner_id)
            unique_owners.add(owner_id)
            owner_id = ''
            continue

        temp_words = line.strip().split()

        if len(temp_words) == 10 and temp_words[0] == 'StateId:' and temp_words[2] == 'Mode:' and temp_words[8] == 'Owner:':
            mode = temp_words[3]
            owner = temp_words[9]
            last_mode[owner] = mode
            unique_modes.add(mode)
            if owner not in all_modes:
                all_modes[owner] = set()
            all_modes[owner].add(mode)
            if mode != 'Closed;':
                active_owner_ids.add(owner)
            continue

        if len(temp_words) > 2 and temp_words[0] == 'Owner' and temp_words[1] == 'Id:':
            have_new_owner_id = True
            owner_id = temp_words[2]
            unique_owner_ids.add(owner_id)
            continue

    for owner in owners:
        if owner in all_modes and all_modes[owner] == just_closed:
            closed_owners.append(owner)
            unique_closed_owners.add(owner)

    multiple_modes = []
    for owner in all_modes:
        if len(all_modes[owner]) > 1:
            # print(owner + ' has multiple modes')
            multiple_modes.append(owner)

    # print('Found ' + str(len(owners)) + ' owners')
    # print('Found ' + str(len(last_mode)) + ' last modes')
    print('Found ' + str(len(closed_owners)) + ' owner IDs with only closed files')
    print('Found ' + str(len(active_owner_ids)) + ' active owner IDs')
    # print('Set unique_owners is ' + str(len(unique_owners)))
    # print('Set unique_owner_ids is ' + str(len(unique_owner_ids)))
    # print('Set unique_closed_owners is ' + str(len(unique_closed_owners)))
    # print('Owners with multiple modes: ' + str(len(multiple_modes)))
    # print('Modes found:')
    # for mode in unique_modes:
        # print(mode)


if __name__ == "__main__":
    main()
