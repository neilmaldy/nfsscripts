import sys
import getopt

verbose = False
debug = False
limit = 10
input_file = None


def usage():
    print('''Usage: lock_status_v_check.py --input=<input_file> [--help] [--limit=<int>] [--verbose] [--debug]
    
    Use --limit=<int> to specify the cutoff limit for displaying lock owners, default is 10''')


try:
    opts, args = getopt.getopt(sys.argv[1:], "vdl:hi:", ["verbose", "debug", "limit=", "help", "input="])
except getopt.GetoptError:
    usage()
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit()
    elif opt in ("-i", "--input"):
        input_file = arg
    elif opt in ("-v", "--verbose"):
        verbose = True
    elif opt in ("-d", "--debug"):
        debug = True
    elif opt in ("-l", "--limit"):
        try:
            limit = int(arg)
        except ValueError:
            print('\nlimit should be an integer, using default limit=10\n')

if input_file:
    f = open(input_file)
    lines = f.read().splitlines()
    f.close()
else:
    print('Please specify input file with --input= option')
    usage()
    sys.exit(2)

in_locking_stateid_dump = False
in_locking_clients_dump = False
in_locking_owners_dump = False

owner_stateids = {}
owner_stateids_not_closed = {}
owner_id_modes = {}

client_stateids = {}
client_stateids_not_closed = {}
client_owners = {}
client_owners_not_closed = {}

client_id_to_ip = {}

locking_stateid_dump_stateid_list = []
stateid_count = 0
owner_list = []
owner_set = set()
client_id_count = 0
client_ip_list = []

modes = set()

for i, line in enumerate(lines):
    if '---------------- V4 Locking StateId Dump ------------------' in line:
        if verbose: print('Parsing ---------------- V4 Locking StateId Dump ------------------')
        in_locking_stateid_dump = True
        in_locking_clients_dump = False
        in_locking_owners_dump = False
        # StateId is unique, owner is not
        continue
    if '---------------- V4 Locking Clients Dump ------------------' in line:
        if verbose: print('Found ' + str(stateid_count) + ' stateid rows and ' + str(len(locking_stateid_dump_stateid_list)) + ' unique stateids')
        if verbose: print('Found following modes: ' + ' '.join(modes))
        if verbose: print('Found ' + str(len(owner_set)) + ' unique owner IDs')
        if verbose: print('Parsing ---------------- V4 Locking Clients Dump ------------------')
        in_locking_stateid_dump = False
        in_locking_clients_dump = True
        in_locking_owners_dump = False
        # client id and id string are unique
        continue
    if '---------------- V4 Locking Owners Dump ------------------' in line:
        if verbose: print('Found ' + str(client_id_count) + ' client id rows and ' + str(len(client_ip_list)) + ' unique client IPs')
        if verbose: print('Parsing ---------------- V4 Locking Owners Dump ------------------')
        in_locking_stateid_dump = False
        in_locking_clients_dump = False
        in_locking_owners_dump = True
        # owner id is not unique

    temp_words = line.strip().split()

    if in_locking_stateid_dump:
        if len(temp_words) == 10 and temp_words[0] == 'StateId:' and temp_words[2] == 'Mode:' and temp_words[8] == 'Owner:' and len(lines) > i + 2:
            stateid_count += 1
            stateid = temp_words[1]
            if stateid in locking_stateid_dump_stateid_list:
                print('Duplicate stateid found: ' + stateid)
            else:
                locking_stateid_dump_stateid_list.append(stateid)
            mode = temp_words[3]
            modes.add(mode)
            owner = temp_words[9]
            client_line = lines[i + 2].strip()
            if 'Client Id' in client_line:
                temp_words = client_line.split()
                if len(temp_words) == 3:
                    client_id = temp_words[2]
                    if client_id not in client_stateids:
                        client_stateids[client_id] = []
                    client_stateids[client_id].append((stateid, mode, owner))

                    if client_id not in client_owners:
                        client_owners[client_id] = set()
                    client_owners[client_id].add(owner)

                    owner_list.append(owner)
                    owner_set.add(owner)
                    if owner not in owner_stateids:
                        owner_stateids[owner] = []
                    owner_stateids[owner].append((stateid, mode, client_id))

                    if owner not in owner_id_modes:
                        owner_id_modes[owner] = set()
                    owner_id_modes[owner].add(mode)


                    if 'Closed' not in mode:
                        if client_id not in client_stateids_not_closed:
                            client_stateids_not_closed[client_id] = []
                        client_stateids_not_closed[client_id].append((stateid, mode, owner))

                        if client_id not in client_owners_not_closed:
                            client_owners_not_closed[client_id] = set()
                        client_owners_not_closed[client_id].add(owner)

                        if owner not in owner_stateids_not_closed:
                            owner_stateids_not_closed[owner] = []
                        owner_stateids_not_closed[owner].append((stateid, mode, client_id))
                else:
                    print('Skipping unexpected client id line ' + client_line)
            else:
                print('Missing client line for ' + line.strip())

    if in_locking_clients_dump:
        if len(temp_words) > 1 and temp_words[0] == 'Client' and temp_words[1] == 'Id' and len(lines) > i + 1:
            client_id_count += 1
            client_id = temp_words[2]
            id_string_line = lines[i + 1].strip()
            if 'Id String:' in id_string_line:
                temp_words = id_string_line.split()
                if len(temp_words) > 3 and "Solaris:" in temp_words[2] or "UNIX" in temp_words[2]:
                    client_ip = temp_words[3]
                    client_id_to_ip[client_id] = client_ip
                elif len(temp_words) > 2 and "Linux" not in id_string_line:
                    client_ip = temp_words[2]
                    client_id_to_ip[client_id] = client_ip
                elif len(temp_words) > 4:
                    client_ip = id_string_line.split()[4]
                    client_id_to_ip[client_id] = client_ip
                else:
                    print('Unexpected ' + id_string_line)

                if (verbose or debug) and client_ip in client_ip_list:
                    print('Found duplicate client ip ' + client_ip + ' in V4 Locking Clients Dump')
                else:
                    client_ip_list.append(client_ip)
            else:
                print('Missing Id String for ' + line.strip())

client_ip_owner_count = {}
client_ip_owner_not_closed_count = {}

for client_id in client_id_to_ip:
    if client_id in client_stateids:
        if debug: print('Client ' + client_id_to_ip[client_id] + ' has ' + str(len(client_owners[client_id])) + ' Owners')
        if client_id_to_ip[client_id] not in client_ip_owner_count:
            client_ip_owner_count[client_id_to_ip[client_id]] = 0
        if client_id_to_ip[client_id] not in client_ip_owner_not_closed_count:
            client_ip_owner_not_closed_count[client_id_to_ip[client_id]] = 0
        client_ip_owner_count[client_id_to_ip[client_id]] += len(client_owners[client_id])
        if client_id in client_owners_not_closed:
            client_ip_owner_not_closed_count[client_id_to_ip[client_id]] += len(client_owners_not_closed[client_id])
        if verbose and len(client_owners[client_id]) > limit:
            if client_id in client_owners_not_closed:
                print('Client ' + client_id + ' with IP ' + client_id_to_ip[client_id] + ' has ' + str(len(client_owners[client_id])) + ' owners, ' + str(len(client_owners_not_closed[client_id])) + ' active')
            else:
                print('Client ' + client_id + ' with IP ' + client_id_to_ip[client_id] + ' has ' + str(len(client_owners[client_id])) + ' owners (all stateids closed)')

for client_ip in sorted(client_ip_owner_count, key=lambda k: client_ip_owner_count[k]):
    if client_ip_owner_count[client_ip] > limit:
        if client_ip in client_ip_owner_not_closed_count and client_ip_owner_not_closed_count[client_ip] > 0:
            print('Client IP ' + client_ip + ' has ' + str(client_ip_owner_count[client_ip]) +' owner ids, ' + str(client_ip_owner_not_closed_count[client_ip]) + ' active')
        else:
            print('Client IP ' + client_ip + ' has ' + str(client_ip_owner_count[client_ip]) + ' owner ids (all stateids closed)')
print('Done.')